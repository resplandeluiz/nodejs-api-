import express from 'express';
import bodyParser from 'body-parser';
import db from './db/db';

// Config, para express app
const app = express();
const context = "http://localhost:4200";

// Parse dos dados enviados
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
	
	


// GET - listar usuário
app.get('/api/v1/users', (req, res) => {
	
	res.setHeader("Access-Control-Allow-Origin", context);
	
	res.status(200).send({
		success: 'true',
		message: 'all users , success!',
		data: db
	})
	
});




//POST - Inserir novo user
app.post('/api/v1/users', (req, res) => {
  
  res.setHeader("Access-Control-Allow-Origin", context);
  
  if(!req.body.nome) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'nome is required'
    });
	
  } else if(!req.body.email) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'email is required'
    });
	
  }else if(!req.body.sexo) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'sexo is required'
    });
	
  }else if(!req.body.telefone) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'telefone is required'
    });
	
  }else if(!req.body.dataNascimento) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'dataNascimento is required'
    });
	
  }
  
 const user = {
		
	id: db.length + 1,  
    nome: req.body.nome,
    email: req.body.email,
	sexo: req.body.sexo,
	telefone: req.body.telefone,
	dataNascimento: req.body.dataNascimento	
	
 }
 
	db.push(user);
	
	
	
	return res.status(201).send({
	   success: 'true',
	   message: 'user added successfully',
	   data: user
	 })
});


// GET - Listar um registro único
app.get('/api/v1/users/:id', (req, res) => {
  
  const id = parseInt(req.params.id, 10);
  res.setHeader("Access-Control-Allow-Origin", context);
  
  db.map((data) => {
    if (data.id === id) {
		
      return res.status(200).send({
        success: 'true',
        message: 'user retrieved successfully',
        data,
      });
    } 
});
 return res.status(404).send({
   success: 'false',
   message: 'user does not exist',
  });
});


//DELETE - Deletar um registro
app.delete('/api/v1/users/:id', (req, res)=>{
	
	const id = parseInt(req.params.id, 10);
	res.setHeader("Access-Control-Allow-Origin", context);
	
	db.map((user,index) =>{
		if(user.id === id){
			db.splice(index, 1);
			
			return res.status(200).send({
				success: 'true',
				message: 'user deleted',				
			});
		}	
		
	});	
	
	return res.status(404).send({
		success: 'false',
		message: 'user does not exist',
		});
	
	
});


//PUT - Atualizar um registro
app.put('/api/v1/users/:id', (req, res) => {
  
  const id = parseInt(req.params.id, 10);
  let userFound;
  let itemIndex;
  res.setHeader("Access-Control-Allow-Origin", context);
  
  
  db.map((data, index) => {
    if (data.id === id) {
      userFound = data;
      itemIndex = index;
    }
  });

  if (!userFound) {
    return res.status(404).send({
      success: 'false',
      message: 'todo not found',
    });
  }

   if(!req.body.nome) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'nome is required'
    });
	
  } else if(!req.body.email) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'email is required'
    });
	
  }else if(!req.body.sexo) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'sexo is required'
    });
	
  }else if(!req.body.telefone) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'telefone is required'
    });
	
  }else if(!req.body.dataNascimento) {
	  
    return res.status(400).send({
      success: 'false',
      message: 'dataNascimento is required'
    });
	
  }

  const updatedTodo = {

    id: 			userFound.id,	
	nome: 			req.body.nome 			|| userFound.nome,
    email: 			req.body.email 			|| userFound.email,
	sexo: 			req.body.sexo 			|| userFound.sexo,
	telefone: 		req.body.telefone 		|| userFound.telefone,
	dataNascimento: req.body.dataNascimento || userFound.dataNascimento,
	
	
  };

  db.splice(itemIndex, 1, updatedTodo);
 
  return res.status(201).send({
	 
    success: 'true',
    message: 'user updated successfully',
    data:updatedTodo,
	
  });
});


const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});
